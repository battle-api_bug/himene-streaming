<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserController extends Controller
{
    public function getById($id){
        $user = User::with(["userType"])->get($id);
        if ($user["soft_delete"] == 1) {
            return Response::json("L'utilisateur a été supprimé", 200);
        } else {
            return Response::json($user, 401);
        }
    }

    public function create(Request $request){
        $user = new User;
        $user = User::userFunction($user, $request);
        return Response::json($user, 100);
    }

    public function update($id, Request $resquest){
        $user = User::find($id);
        $user = User::userFunction($user, $request);
        return Response::json($user, 200);
    }

    public function delete($id){
        User::delete($id);
        return Response::json("Deleted", 200);
    }

    public function login(Request $request){
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('Laravel')->accessToken; 
            $success['user_type_id'] =  $user->user_type_id;
            return response()->json($success, 200); 
        } else { 
            return response()->json(['error'=>'Acces refuser'], 401); 
        }
    }

    public function logout() {
        if (Auth::check()):
            Auth::user()->token()->delete();
        endif;
        return Response::json("Déconnexion", 200);
    }
}
