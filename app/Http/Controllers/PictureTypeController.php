<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use App\Models\Picture;

class PictureTypeController extends Controller
{
    public function getById($id){
        $picture = Picture::get($id);
        return Response::json($picture, 200);
    }

    public function create(Request $request){
        $picture = new Picture;
        $request->link = $picture->link;
        $request->picture_type = $picture->picture_type;
        return Response::json($picture, 200);
    }
    
    public function update($id, Request $resquest){
        $picture = Picture::find($id);
        $picture->link = $request->link;
        $picture->picture_type = $request->picture_type;
        return Response::json($picture, 200);
    }

    public function delete($id){
        Picture::destroy($id);
        return Response::json("Deleted", 200);
    }
}
