<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\UserTypeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('login', [UserController::class, 'login']);
Route::get('logout', [UserController::class, 'logout']);

Route::get('albums', [AlbumController::class, 'getAll']);
Route::get('albums/{id}', [AlbumController::class, 'getById']);
Route::get('artistes', [ArtistController::class, 'getAll']);
Route::get('artistes/{id}', [ArtistController::class, 'getById']);
Route::get('musiques', [MusicController::class, 'getAll']);
Route::get('musiques/{id}', [MusicController::class, 'getById']);

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('users/{id}', [UserController::class, 'getById']);
    Route::get('usersTrainer', [UserController::class, 'getAllByTrainer']);
    Route::post('users', [UserController::class, 'create']);
    Route::post('users/{id}', [UserController::class, 'update']);
    Route::delete('users/{id}', [UserController::class, 'delete']);
    
    Route::get('userTypes', [UserTypeController::class, 'getAll']);
    Route::get('userTypes/{id}', [UserTypeController::class, 'getById']);
    Route::post('userTypes', [UserTypeController::class, 'create']);
    Route::post('userTypes/{id}', [UserTypeController::class, 'update']);
    Route::delete('userTypes/{id}', [UserTypeController::class, 'delete']);
    
    Route::post('albums', [AlbumController::class, 'create']);
    Route::post('albums/{id}', [AlbumController::class, 'update']);
    Route::delete('albums/{id}', [AlbumController::class, 'delete']);

    Route::post('artistes', [ArtistController::class, 'create']);
    Route::post('artistes/{id}', [ArtistController::class, 'update']);
    Route::delete('artistes/{id}', [ArtistController::class, 'delete']);

    Route::post('musiques', [MusicController::class, 'create']);
    Route::post('musiques/{id}', [MusicController::class, 'update']);
    Route::delete('musiques/{id}', [MusicController::class, 'delete']);

    Route::post('images', [PictureController::class, 'create']);
    Route::post('images/{id}', [PictureController::class, 'update']);
    Route::delete('images/{id}', [PictureController::class, 'delete']);
});