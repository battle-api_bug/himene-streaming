<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlbumSong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_song', function (Blueprint $table) {
            $table->unsignedBigInteger('noname_f_id')->unsigned()->index();
            $table->foreign('noname_f_id')->references('id')->on('albums')->onDelete('cascade');
            
            $table->unsignedBigInteger('noname_s_id')->unsigned()->index();
            $table->foreign('noname_s_id')->references('id')->on('music')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_song');
    }
}
