<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Album;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $albums = [
            [
                "name" => "Trio kikiriri, Vol. 2",
                "year" => "2013-01-01",
                "copyright" => "℗ 2012 Studio Alphonse - 2012",
                "artiste_id" => 1,
                "picture_id" => 5,
                "album_id" => 5,
            ],
            [
                "name" => "Tu e Popo - Single",
                "year" => "2013-01-01",
                "copyright" => "℗ 2012 Studio Alphonse - 2012",
                "artiste_id" => 1,
                "picture_id" => 6,
                "album_id" => 5,
            ],
        ];

        foreach($albums AS $album):
            Album::create($album);
        endforeach;
    }
}
