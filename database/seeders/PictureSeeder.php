<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Picture;

class PictureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pictures = [
            [
                "link" => "trio_kikiriri.png",
                "picture_type_id" => 1,
            ],
            [
                "link" => "manahune.png",
                "picture_type_id" => 1,
            ],
            [
                "link" => "angelo_neuffer.png",
                "picture_type_id" => 1,
            ],
            [
                "link" => "sabrina_laughlin.png",
                "picture_type_id" => 1,
            ],
            [
                "link" => "trio_kikiriri_vol_2.png",
                "picture_type_id" => 1,
            ],
            [
                "link" => "tu-e-popo.png",
                "picture_type_id" => 4,
            ],
        ];

        foreach($picture AS $pictures):
            Picture::create($pictures);
        endforeach;
    }
}
