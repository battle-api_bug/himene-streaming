<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Artist;

class ArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $artistes = [
            [
                "name" => "Trio Kikiriri",
                "picture_id" => 1,
            ],
            [
                "name" => "Manahune",
                "picture_id" => 3,
            ],
            [
                "name" => "Angelo Neuffer",
                "picture_id" => 3,
            ],
            [
                "name" => "Sabrina Laughlin",
                "picture_id" => 4,
            ],
            [
                "name" => "Bobby Holcomb",
                "picture_id" => 5,
            ],
            [
                "name" => "Koru",
                "picture_id" => 2,
            ],
        ];

        foreach($artistes as $artiste):
            Artist::create($artiste);
        endforeach;
    }
}
