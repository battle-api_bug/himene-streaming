<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PictureTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ["name" => "Photo de profil"],
            ["name" => "Album de couverture"],
            ["name" => "Couverture de playlist"],
            ["name" => "Single"],
        ];

        foreach($types AS $type):
            PictureType::create($type);
        endforeach;
    }
}
