<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                "lastname" => "Terrieur",
                "firstname" => "Alain",
                "email" => "alainterrieur@gmail.com",
                "password" => "alEx1989",
            ],
            [
                "lastname" => "AGNIERAY",
                "firstname" => "Tehei",
                "email" => "tehei@gmail.com",
                "password" => "jesuisleBOSSducode",
            ],
            [
                "lastname" => "Ton nom de famille",
                "firstname" => "Ton prénom",
                "email" => "Ton adresse électronique",
                "password" => "Ton mot de passe",
            ],
        ];

        for ($i=0; $i < 1; $i++) { 
            User::create($users[$i]);
        }
    }
}
