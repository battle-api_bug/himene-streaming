<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Music;

class MusicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $musics = [
            [
                "title" => "Te Taata Hara Nei / Anana Na To Mata / Na Oe Vairea / Te Moa O Te Taurea",
                "duration" => "7:34",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "Moi, Les Filles Je Les Aime / Taiarapu",
                "duration" => "6:33",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "Vahi Ateroa / Ire Ae Te Pahi / Taime Haumaru / Tiare Hinano",
                "duration" => "5:04",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "Tarona / I'M Singing For Happiness",
                "duration" => "4:21",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "Les Filles De Chez Moi",
                "duration" => "3:57",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "Vahine Kaina / Ua Tangi Te Kukupa / Kaina Ote Toau Toku Henua / E Tamarii Otare / Tamure",
                "duration" => "5:32",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "Elle",
                "duration" => "3:13",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "E Hoa Tau I Here / Maui Girl",
                "duration" => "5:02",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "Cette Annee / Corps A Corps",
                "duration" => "5:46",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "N'Aimer Que Toi",
                "duration" => "5:51",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
            [
                "title" => "Elle Prefere L'Amour En Mer",
                "duration" => "3:41",
                "lyrics" => "",
                "artiste_id" => 1,
                "picture_id" => "5",
            ],
        ];

        foreach($musics AS $music):
            Music::create($music);
        endforeach;
    }
}
